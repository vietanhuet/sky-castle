#ifndef OBJ_PARSER_H
#define OBJ_PARSER_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <assert.h>
#include <stdio.h>
#include <cstring>
#include <vector>

#define OBJ_FILENAME_LENGTH 500
#define MATERIAL_NAME_SIZE 255
#define OBJ_LINE_SIZE 500
#define MAX_VERTEX_COUNT 4
using namespace std;
typedef struct{
	int item_count;
	int current_max_size;
	char growable;

	void **items;
	char **names;
} List;

void list_make(List *listo, int size, char growable);
int list_add_item(List *listo, void *item, char *name);
char* list_print_items(List *listo);
void* list_get_name(List *listo, char *name);
void* list_get_index(List *listo, int indx);
void* list_get_item(List *listo, void *item_to_find);
int list_find(List *listo, char *name_to_find);
void list_delete_index(List *listo, int indx);
void list_delete_name(List *listo, char *name);
void list_delete_item(List *listo, void *item);
void list_delete_all(List *listo);
void list_print_list(List *listo);
void list_free(List *listo);
void test_list();

typedef	struct {
	GLubyte	* imageData;									// Image Data (Up To 32 Bits)
	GLuint	bpp;											// Image Color Depth In Bits Per Pixel
	GLuint	width;											// Image Width
	GLuint	height;											// Image Height
	GLuint	texID;											// Texture ID Used To Select A Texture
	GLuint	type;											// Image Type (GL_RGB, GL_RGBA)
} Texture;
typedef struct ObjFace {
	int vertex_index[MAX_VERTEX_COUNT];
	int normal_index[MAX_VERTEX_COUNT];
	int texture_index[MAX_VERTEX_COUNT];
	int vertex_count;
	int material_index;
};
typedef struct ObjSphere {
	int pos_index;
	int up_normal_index;
	int equator_normal_index;
	int texture_index[MAX_VERTEX_COUNT];
	int material_index;
};
typedef struct ObjPlane {
	int pos_index;
	int normal_index;
	int rotation_normal_index;
	int texture_index[MAX_VERTEX_COUNT];
	int material_index;
};
typedef struct ObjVector {
	double e[3];
};
typedef struct ObjMaterial {
	char name[MATERIAL_NAME_SIZE];
	char texture_filename[OBJ_FILENAME_LENGTH];
	double amb[3];
	double diff[3];
	double spec[3];
	double reflect;
	double refract;
	double trans;
	double shiny;
	double glossy;
	double refract_index;
};
typedef struct ObjCamera {
	int camera_pos_index;
	int camera_look_point_index;
	int camera_up_norm_index;
};
typedef struct ObjLightPoint {
	int pos_index;
	int material_index;
};
typedef struct ObjLightDisc {
	int pos_index;
	int normal_index;
	int material_index;
};
typedef struct ObjLightQuad {
	int vertex_index[MAX_VERTEX_COUNT];
	int material_index;
};
typedef struct ObjGrowableSceneData {
	char scene_filename[OBJ_FILENAME_LENGTH];
	char material_filename[OBJ_FILENAME_LENGTH];

	List vertex_list;
	List vertex_normal_list;
	List vertex_texture_list;

	List face_list;
	List sphere_list;
	List plane_list;

	List light_point_list;
	List light_quad_list;
	List light_disc_list;

	List material_list;

	ObjCamera *camera;
};

typedef struct ObjSceneData {
	ObjVector **vertex_list;
	ObjVector **vertex_normal_list;
	ObjVector **vertex_texture_list;

	ObjFace **face_list;
	ObjSphere **sphere_list;
	ObjPlane **plane_list;

	ObjLightPoint **light_point_list;
	ObjLightQuad **light_quad_list;
	ObjLightDisc **light_disc_list;

	ObjMaterial **material_list;

	int vertex_count;
	int vertex_normal_count;
	int vertex_texture_count;

	int face_count;
	int sphere_count;
	int plane_count;

	int light_point_count;
	int light_quad_count;
	int light_disc_count;

	int material_count;

	ObjCamera *camera;
};

int parse_obj_scene(ObjSceneData *data_out, char *filename);
void delete_obj_data(ObjSceneData *data_out);

class Coordinates {
public:
    GLfloat x, y, z;
    Coordinates(GLfloat x, GLfloat y, GLfloat z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

class ObjLoader{
public:
	ObjLoader() {}
	~ObjLoader(){
		delete_obj_data(&data);
	}
	ObjLoader(char* filename, Coordinates* coor, GLfloat minimizeRate, GLfloat angel) {
        load(filename);
        this->coor = coor;
        this->minimizeRate = minimizeRate;
        this->angel = angel;
	}

    Coordinates* coor;
    GLfloat minimizeRate;
    GLfloat angel; // goc lech so voi truc x
    GLuint objTexture[1000];

	int load(char *filename);

	ObjVector **vertexList;
	ObjVector **normalList;
	ObjVector **textureList;

	ObjFace **faceList;
	ObjSphere **sphereList;
	ObjPlane **planeList;

	ObjLightPoint **lightPointList;
	ObjLightQuad **lightQuadList;
	ObjLightDisc **lightDiscList;

	ObjMaterial **materialList;

	unsigned long long vertexCount;
	unsigned long long normalCount;
	unsigned long long textureCount;

	unsigned long long faceCount;
	unsigned long long sphereCount;
	unsigned long long planeCount;

	unsigned long long lightPointCount;
	unsigned long long lightQuadCount;
	unsigned long long lightDiscCount;

	unsigned long long materialCount;

	ObjCamera *camera;
	ObjSceneData data;
};
// string
char strequal(const char *s1, const char *s2);
char contains(const char *haystack, const char *needle);


class RgbImage {
public:
   RgbImage();
   RgbImage(const char* filename);
   RgbImage(unsigned long numRows, unsigned long numCols);
   ~RgbImage();

   bool LoadBmpFile(const char *);
   bool WriteBmpFile( const char* filename );      // Write the bitmap to the specified file
#ifndef RGBIMAGE_DONT_USE_OPENGL
   bool LoadFromOpenglBuffer();               // Load the bitmap from the current OpenGL buffer
#endif

   long GetNumRows() const { return NumRows; }
   long GetNumCols() const { return NumCols; }
   // Rows are word aligned
   long GetNumBytesPerRow() const { return ((3*NumCols+3)>>2)<<2; }
   const void* ImageData() const { return (void*)ImagePtr; }

   const unsigned char* GetRgbPixel( long row, long col ) const;
   unsigned char* GetRgbPixel( long row, long col );
   void GetRgbPixel( long row, long col, float* red, float* green, float* blue ) const;
   void GetRgbPixel( long row, long col, double* red, double* green, double* blue ) const;

   void SetRgbPixelf( long row, long col, double red, double green, double blue );
   void SetRgbPixelc( long row, long col, unsigned char red, unsigned char green, unsigned char blue );

   // Error reporting. (errors also print message to stderr)
   int GetErrorCode() const { return ErrorCode; }
   enum {
      NoError = 0,
      OpenError = 1,         // Unable to open file for reading
      FileFormatError = 2,   // Not recognized as a 24 bit BMP file
      MemoryError = 3,      // Unable to allocate memory for image data
      ReadError = 4,         // End of file reached prematurely
      WriteError = 5         // Unable to write out data (or no date to write out)
   };
   bool ImageLoaded() const { return (ImagePtr!=0); }  // Is an image loaded?
   void Reset();         // Frees image data memory

    unsigned char* ImagePtr;
    long NumRows;
    long NumCols;
    int ErrorCode;

    static short readShort(FILE*);
    static long readLong(FILE*);
    static void skipChars(FILE*, int);
    static void writeLong(long, FILE*);
    static void writeShort(short, FILE*) ;
    static unsigned char doubleToUnsignedChar(double);
};

// util
void printVector(ObjVector*);
void printObjDetails(ObjLoader*);
void loadTextureFromFile(ObjLoader*, char *, int);
void loadTextureToObject(ObjLoader*);
void createObject(vector<ObjLoader*>&, char*, Coordinates*, GLfloat, GLfloat);
void *playSound(void *);
void startSound();
bool enterFullscreen(HWND, int, int, int, int);
bool exitFullscreen(HWND, int, int, int, int, int, int);

#endif
