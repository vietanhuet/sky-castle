#include <iostream>
#include <vector>
#include <windows.h>
#include <cstdlib>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glut.h>
#include <fstream>
#include <sstream>
#include <mmsystem.h>
#include <pthread.h>
#include "SkyCity.h"
#define WHITESPACE " \t\n\r"
using namespace std;

void obj_free_half_list(List *listo){
	list_delete_all(listo);
	free(listo->names);
}
int obj_convert_to_list_index(int current_max, int index){
	if(index == 0)  //no index
		return -1;

	if(index < 0)  //relative to current List position
		return current_max + index;

	return index - 1;  //normal counting index
}
void obj_convert_to_list_index_v(int current_max, int *indices){
	for(int i=0; i<MAX_VERTEX_COUNT; i++)
		indices[i] = obj_convert_to_list_index(current_max, indices[i]);
}
void obj_set_material_defaults(ObjMaterial *mtl){
	mtl->amb[0] = 0.2;
	mtl->amb[1] = 0.2;
	mtl->amb[2] = 0.2;
	mtl->diff[0] = 0.8;
	mtl->diff[1] = 0.8;
	mtl->diff[2] = 0.8;
	mtl->spec[0] = 1.0;
	mtl->spec[1] = 1.0;
	mtl->spec[2] = 1.0;
	mtl->reflect = 0.0;
	mtl->trans = 1;
	mtl->glossy = 98;
	mtl->shiny = 0;
	mtl->refract_index = 1;
	mtl->texture_filename[0] = '\0';
}
int obj_parse_vertex_index(int *vertex_index, int *texture_index, int *normal_index){
	char *temp_str;
	char *token;
	int vertex_count = 0;
	while( (token = strtok(NULL, WHITESPACE)) != NULL){
		if(texture_index != NULL)
			texture_index[vertex_count] = 0;
		if(normal_index != NULL)
		normal_index[vertex_count] = 0;

		vertex_index[vertex_count] = atoi( token );

		if(contains(token, "//"))  //normal only
		{
			temp_str = strchr(token, '/');
			temp_str++;
			normal_index[vertex_count] = atoi( ++temp_str );
		}
		else if(contains(token, "/")){
			temp_str = strchr(token, '/');
			texture_index[vertex_count] = atoi( ++temp_str );

			if(contains(temp_str, "/")){
				temp_str = strchr(temp_str, '/');
				normal_index[vertex_count] = atoi( ++temp_str );
			}
		}

		vertex_count++;
	}

	return vertex_count;
}
ObjFace* obj_parse_face(ObjGrowableSceneData *scene){
	int vertex_count;
	ObjFace *face = (ObjFace*)malloc(sizeof(ObjFace));
	vertex_count = obj_parse_vertex_index(face->vertex_index, face->texture_index, face->normal_index);
	obj_convert_to_list_index_v(scene->vertex_list.item_count, face->vertex_index);
	obj_convert_to_list_index_v(scene->vertex_texture_list.item_count, face->texture_index);
	obj_convert_to_list_index_v(scene->vertex_normal_list.item_count, face->normal_index);
	face->vertex_count = vertex_count;

	return face;
}
ObjSphere* obj_parse_sphere(ObjGrowableSceneData *scene){
	int temp_indices[MAX_VERTEX_COUNT];
	ObjSphere *obj = (ObjSphere*)malloc(sizeof(ObjSphere));
	obj_parse_vertex_index(temp_indices, obj->texture_index, NULL);
	obj_convert_to_list_index_v(scene->vertex_texture_list.item_count, obj->texture_index);
	obj->pos_index = obj_convert_to_list_index(scene->vertex_list.item_count, temp_indices[0]);
	obj->up_normal_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, temp_indices[1]);
	obj->equator_normal_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, temp_indices[2]);
	return obj;
}
ObjPlane* obj_parse_plane(ObjGrowableSceneData *scene){
	int temp_indices[MAX_VERTEX_COUNT];
	ObjPlane *obj = (ObjPlane*)malloc(sizeof(ObjPlane));
	obj_parse_vertex_index(temp_indices, obj->texture_index, NULL);
	obj_convert_to_list_index_v(scene->vertex_texture_list.item_count, obj->texture_index);
	obj->pos_index = obj_convert_to_list_index(scene->vertex_list.item_count, temp_indices[0]);
	obj->normal_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, temp_indices[1]);
	obj->rotation_normal_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, temp_indices[2]);
	return obj;
}
ObjLightPoint* obj_parse_light_point(ObjGrowableSceneData *scene){
	ObjLightPoint *o= (ObjLightPoint*)malloc(sizeof(ObjLightPoint));
	o->pos_index = obj_convert_to_list_index(scene->vertex_list.item_count, atoi( strtok(NULL, WHITESPACE)) );
	return o;
}
ObjLightQuad* obj_parse_light_quad(ObjGrowableSceneData *scene){
	ObjLightQuad *o = (ObjLightQuad*)malloc(sizeof(ObjLightQuad));
	obj_parse_vertex_index(o->vertex_index, NULL, NULL);
	obj_convert_to_list_index_v(scene->vertex_list.item_count, o->vertex_index);
	return o;
}
ObjLightDisc* obj_parse_light_disc(ObjGrowableSceneData *scene){
	int temp_indices[MAX_VERTEX_COUNT];
	ObjLightDisc *obj = (ObjLightDisc*)malloc(sizeof(ObjLightDisc));
	obj_parse_vertex_index(temp_indices, NULL, NULL);
	obj->pos_index = obj_convert_to_list_index(scene->vertex_list.item_count, temp_indices[0]);
	obj->normal_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, temp_indices[1]);
	return obj;
}
ObjVector* obj_parse_vector(){
	ObjVector *v = (ObjVector*)malloc(sizeof(ObjVector));
    char *e0 = strtok(NULL, WHITESPACE);
    char *e1 = strtok(NULL, WHITESPACE);
    char *e2 = strtok(NULL, WHITESPACE);
	v->e[0] = e0 ? atof(e0) : 0.0;
	v->e[1] = e1 ? atof(e1) : 0.0;
    v->e[2] = e2 ? atof(e2) : 0.0;
	return v;
}
void obj_parse_camera(ObjGrowableSceneData *scene, ObjCamera *camera){
	int indices[3];
	obj_parse_vertex_index(indices, NULL, NULL);
	camera->camera_pos_index = obj_convert_to_list_index(scene->vertex_list.item_count, indices[0]);
	camera->camera_look_point_index = obj_convert_to_list_index(scene->vertex_list.item_count, indices[1]);
	camera->camera_up_norm_index = obj_convert_to_list_index(scene->vertex_normal_list.item_count, indices[2]);
}
int obj_parse_mtl_file(char *filename, List *material_list){
	int line_number = 0;
	char *current_token;
	char current_line[OBJ_LINE_SIZE];
	char material_open = 0;
	ObjMaterial *current_mtl = NULL;
	FILE *mtl_file_stream;
	// open scene
	mtl_file_stream = fopen( filename, "r");
	if(mtl_file_stream == 0){
		fprintf(stderr, "Error reading file: %s\n", filename);
		return 0;
	}
	list_make(material_list, 10, 1);
	while( fgets(current_line, OBJ_LINE_SIZE, mtl_file_stream) ){
		current_token = strtok( current_line, " \t\n\r");
		line_number++;
		//skip comments
		if( current_token == NULL || strequal(current_token, "//") || strequal(current_token, "#"))
			continue;
		//start material
		else if( strequal(current_token, "newmtl")){
			material_open = 1;
			current_mtl = (ObjMaterial*) malloc(sizeof(ObjMaterial));
			obj_set_material_defaults(current_mtl);
			// get the name
			strncpy(current_mtl->name, strtok(NULL, " \t"), MATERIAL_NAME_SIZE);
			list_add_item(material_list, current_mtl, current_mtl->name);
		}
		//ambient
		else if( strequal(current_token, "Ka") && material_open){
			current_mtl->amb[0] = atof( strtok(NULL, " \t"));
			current_mtl->amb[1] = atof( strtok(NULL, " \t"));
			current_mtl->amb[2] = atof( strtok(NULL, " \t"));
		}
		//diff
		else if( strequal(current_token, "Kd") && material_open){
			current_mtl->diff[0] = atof( strtok(NULL, " \t"));
			current_mtl->diff[1] = atof( strtok(NULL, " \t"));
			current_mtl->diff[2] = atof( strtok(NULL, " \t"));
		}
		//specular
		else if( strequal(current_token, "Ks") && material_open){
			current_mtl->spec[0] = atof( strtok(NULL, " \t"));
			current_mtl->spec[1] = atof( strtok(NULL, " \t"));
			current_mtl->spec[2] = atof( strtok(NULL, " \t"));
		}
		//shiny
		else if( strequal(current_token, "Ns") && material_open){
			current_mtl->shiny = atof( strtok(NULL, " \t"));
		}
		//transparent
		else if( strequal(current_token, "d") && material_open){
			current_mtl->trans = atof( strtok(NULL, " \t"));
		}
		//reflection
		else if( strequal(current_token, "r") && material_open){
			current_mtl->reflect = atof( strtok(NULL, " \t"));
		}
		//glossy
		else if( strequal(current_token, "sharpness") && material_open){
			current_mtl->glossy = atof( strtok(NULL, " \t"));
		}
		//refract index
		else if( strequal(current_token, "Ni") && material_open){
			current_mtl->refract_index = atof( strtok(NULL, " \t"));
		}
		// illumination type
		else if( strequal(current_token, "illum") && material_open){
		}
		// texture map
		else if( strequal(current_token, "map_Kd") && material_open){
			strncpy(current_mtl->texture_filename, strtok(NULL, " \t"), OBJ_FILENAME_LENGTH);
		}
		else{
			fprintf(stderr, "Unknown command '%s' in material file %s at line %i:\n\t%s\n", current_token, filename, line_number, current_line);
			//return 0;
		}
	}

	fclose(mtl_file_stream);
	return 1;
}
int obj_parse_obj_file(ObjGrowableSceneData *growable_data, char *filename){
	FILE* obj_file_stream;
	int current_material = -1;
	char *current_token = NULL;
	char current_line[OBJ_LINE_SIZE];
	int line_number = 0;
	// open scene
	obj_file_stream = fopen( filename, "r");
	if(obj_file_stream == 0){
		fprintf(stderr, "Error reading file: %s\n", filename);
		return 0;
	}
	//parser loop
	while( fgets(current_line, OBJ_LINE_SIZE, obj_file_stream) ){
		current_token = strtok( current_line, " \t\n\r");
		//skip comments
		if( current_token == NULL || current_token[0] == '#')
			continue;
		//parse objects
		else if( strequal(current_token, "v") ) //process vertex
        {
			list_add_item(&growable_data->vertex_list,  obj_parse_vector(), NULL);
		}
		else if( strequal(current_token, "vn") ) //process vertex normal
		{
			list_add_item(&growable_data->vertex_normal_list,  obj_parse_vector(), NULL);
		}
		else if( strequal(current_token, "vt") ) //process vertex texture
		{
			list_add_item(&growable_data->vertex_texture_list,  obj_parse_vector(), NULL);
		}
		else if( strequal(current_token, "f") ) //process face
		{
			ObjFace *face = obj_parse_face(growable_data);
			face->material_index = current_material;
			list_add_item(&growable_data->face_list, face, NULL);
		}
		else if( strequal(current_token, "sp") ) //process sphere
		{
			ObjSphere *sphr = obj_parse_sphere(growable_data);
			sphr->material_index = current_material;
			list_add_item(&growable_data->sphere_list, sphr, NULL);
		}

		else if( strequal(current_token, "pl") ) //process plane
		{
			ObjPlane *pl = obj_parse_plane(growable_data);
			pl->material_index = current_material;
			list_add_item(&growable_data->plane_list, pl, NULL);
		}
		else if( strequal(current_token, "p") ) //process point
		{
			//make a small sphere to represent the point?
		}
		else if( strequal(current_token, "lp") ) //light point source
		{
			ObjLightPoint *o = obj_parse_light_point(growable_data);
			o->material_index = current_material;
			list_add_item(&growable_data->light_point_list, o, NULL);
		}
		else if( strequal(current_token, "ld") ) //process light disc
		{
			ObjLightDisc *o = obj_parse_light_disc(growable_data);
			o->material_index = current_material;
			list_add_item(&growable_data->light_disc_list, o, NULL);
		}
		else if( strequal(current_token, "lq") ) //process light quad
		{
			ObjLightQuad *o = obj_parse_light_quad(growable_data);
			o->material_index = current_material;
			list_add_item(&growable_data->light_quad_list, o, NULL);
		}
		else if( strequal(current_token, "c") ) //camera
		{
			growable_data->camera = (ObjCamera*) malloc(sizeof(ObjCamera));
			obj_parse_camera(growable_data, growable_data->camera);
		}
		else if( strequal(current_token, "usemtl") ) // usemtl
		{
			current_material = list_find(&growable_data->material_list, strtok(NULL, WHITESPACE));
		}
		else if( strequal(current_token, "mtllib") ) // mtllib
		{
			strncpy(growable_data->material_filename, strtok(NULL, WHITESPACE), OBJ_FILENAME_LENGTH);
			obj_parse_mtl_file(growable_data->material_filename, &growable_data->material_list);
			continue;
		}
		else if( strequal(current_token, "o") ) //object name
		{ }
		else if( strequal(current_token, "s") ) //smoothing
		{ }
		else if( strequal(current_token, "g") ) // group
		{ }
		else
		{
			printf("Unknown command '%s' in scene code at line %i: \"%s\".\n",
					current_token, line_number, current_line);
		}
        line_number++;
	}
	fclose(obj_file_stream);
	return 1;
}
void obj_init_temp_storage(ObjGrowableSceneData *growable_data) {
	list_make(&growable_data->vertex_list, 10, 1);
	list_make(&growable_data->vertex_normal_list, 10, 1);
	list_make(&growable_data->vertex_texture_list, 10, 1);
	list_make(&growable_data->face_list, 10, 1);
	list_make(&growable_data->sphere_list, 10, 1);
	list_make(&growable_data->plane_list, 10, 1);
	list_make(&growable_data->light_point_list, 10, 1);
	list_make(&growable_data->light_quad_list, 10, 1);
	list_make(&growable_data->light_disc_list, 10, 1);
	list_make(&growable_data->material_list, 10, 1);
	growable_data->camera = NULL;
}
void obj_free_temp_storage(ObjGrowableSceneData *growable_data) {
	obj_free_half_list(&growable_data->vertex_list);
	obj_free_half_list(&growable_data->vertex_normal_list);
	obj_free_half_list(&growable_data->vertex_texture_list);
	obj_free_half_list(&growable_data->face_list);
	obj_free_half_list(&growable_data->sphere_list);
	obj_free_half_list(&growable_data->plane_list);
	obj_free_half_list(&growable_data->light_point_list);
	obj_free_half_list(&growable_data->light_quad_list);
	obj_free_half_list(&growable_data->light_disc_list);
	obj_free_half_list(&growable_data->material_list);
}
void delete_obj_data(ObjSceneData *data_out) {
	int i;
	for(i=0; i<data_out->vertex_count; i++)
		free(data_out->vertex_list[i]);
	free(data_out->vertex_list);
	for(i=0; i<data_out->vertex_normal_count; i++)
		free(data_out->vertex_normal_list[i]);
	free(data_out->vertex_normal_list);
	for(i=0; i<data_out->vertex_texture_count; i++)
		free(data_out->vertex_texture_list[i]);
	free(data_out->vertex_texture_list);
	for(i=0; i<data_out->face_count; i++)
		free(data_out->face_list[i]);
	free(data_out->face_list);
	for(i=0; i<data_out->sphere_count; i++)
		free(data_out->sphere_list[i]);
	free(data_out->sphere_list);
	for(i=0; i<data_out->plane_count; i++)
		free(data_out->plane_list[i]);
	free(data_out->plane_list);
	for(i=0; i<data_out->light_point_count; i++)
		free(data_out->light_point_list[i]);
	free(data_out->light_point_list);
	for(i=0; i<data_out->light_disc_count; i++)
		free(data_out->light_disc_list[i]);
	free(data_out->light_disc_list);
	for(i=0; i<data_out->light_quad_count; i++)
		free(data_out->light_quad_list[i]);
	free(data_out->light_quad_list);
	for(i=0; i<data_out->material_count; i++)
		free(data_out->material_list[i]);
	free(data_out->material_list);
	free(data_out->camera);
}
void obj_copy_to_out_storage(ObjSceneData *data_out, ObjGrowableSceneData *growable_data) {
	data_out->vertex_count = growable_data->vertex_list.item_count;
	data_out->vertex_normal_count = growable_data->vertex_normal_list.item_count;
	data_out->vertex_texture_count = growable_data->vertex_texture_list.item_count;
	data_out->face_count = growable_data->face_list.item_count;
	data_out->sphere_count = growable_data->sphere_list.item_count;
	data_out->plane_count = growable_data->plane_list.item_count;
	data_out->light_point_count = growable_data->light_point_list.item_count;
	data_out->light_disc_count = growable_data->light_disc_list.item_count;
	data_out->light_quad_count = growable_data->light_quad_list.item_count;
	data_out->material_count = growable_data->material_list.item_count;
	data_out->vertex_list = (ObjVector**)growable_data->vertex_list.items;
	data_out->vertex_normal_list = (ObjVector**)growable_data->vertex_normal_list.items;
	data_out->vertex_texture_list = (ObjVector**)growable_data->vertex_texture_list.items;
	data_out->face_list = (ObjFace**)growable_data->face_list.items;
	data_out->sphere_list = (ObjSphere**)growable_data->sphere_list.items;
	data_out->plane_list = (ObjPlane**)growable_data->plane_list.items;
	data_out->light_point_list = (ObjLightPoint**)growable_data->light_point_list.items;
	data_out->light_disc_list = (ObjLightDisc**)growable_data->light_disc_list.items;
	data_out->light_quad_list = (ObjLightQuad**)growable_data->light_quad_list.items;
	data_out->material_list = (ObjMaterial**)growable_data->material_list.items;
	data_out->camera = growable_data->camera;
}
int parse_obj_scene(ObjSceneData *data_out, char *filename){
	ObjGrowableSceneData growable_data;
	obj_init_temp_storage(&growable_data);
	if( obj_parse_obj_file(&growable_data, filename) == 0) return 0;
	obj_copy_to_out_storage(data_out, &growable_data);
	obj_free_temp_storage(&growable_data);
	return 1;
}

int ObjLoader::load(char *filename) {
	int no_error = 1;
	no_error = parse_obj_scene(&data, filename);
	if(no_error) {
		this->vertexCount = data.vertex_count;
		this->normalCount = data.vertex_normal_count;
		this->textureCount = data.vertex_texture_count;
		this->faceCount = data.face_count;
		this->sphereCount = data.sphere_count;
		this->planeCount = data.plane_count;
		this->lightPointCount = data.light_point_count;
		this->lightDiscCount = data.light_disc_count;
		this->lightQuadCount = data.light_quad_count;
		this->materialCount = data.material_count;
		this->vertexList = data.vertex_list;
		this->normalList = data.vertex_normal_list;
		this->textureList = data.vertex_texture_list;
		this->faceList = data.face_list;
		this->sphereList = data.sphere_list;
		this->planeList = data.plane_list;
		this->lightPointList = data.light_point_list;
		this->lightDiscList = data.light_disc_list;
		this->lightQuadList = data.light_quad_list;
		this->materialList = data.material_list;
		this->camera = data.camera;
	}
	return no_error;
}
// String extra
char strequal(const char *s1, const char *s2){
	if(strcmp(s1, s2) == 0) return 1;
	return 0;
}
char contains(const char *haystack, const char *needle) {
	if(strstr(haystack, needle) == NULL) return 0;
	return 1;
}

// List
char list_is_full(List *listo){
	return(listo->item_count == listo->current_max_size);
}
void list_grow(List *old_listo) {
	int i;
	List new_listo;
	list_make(&new_listo, old_listo->current_max_size*2, old_listo->growable++);
	for(i=0; i<old_listo->current_max_size; i++) {
		list_add_item(&new_listo, old_listo->items[i] , old_listo->names[i]);
	}
	list_free(old_listo);
	//copy new structure to old List
	old_listo->names = new_listo.names;
	old_listo->items = new_listo.items;
	old_listo->item_count = new_listo.item_count;
	old_listo->current_max_size = new_listo.current_max_size;
	old_listo->growable = new_listo.growable;
}
//end helpers
void list_make(List *listo, int start_size, char growable) {
	listo->names = (char**) malloc(sizeof(char*) * start_size);
	listo->items = (void**) malloc(sizeof(void*) * start_size);
	listo->item_count = 0;
	listo->current_max_size = start_size;
	listo->growable = growable;
}
int list_add_item(List *listo, void *item, char *name){
	int name_length;
	char *new_name;
	if(list_is_full(listo)){
		if( listo->growable ) {
			list_grow(listo);
		} else {
		    return -1;
		}
	}
	listo->names[listo->item_count] = NULL;
	if(name != NULL){
		name_length = strlen(name);
		new_name = (char*) malloc(sizeof(char) * name_length + 1);
		strncpy(new_name, name, name_length);
		listo->names[listo->item_count] = new_name;
	}
	listo->items[listo->item_count] = item;
	listo->item_count++;
	return listo->item_count-1;
}
char* list_print_items(List *listo){
	for(int i=0; i < listo->item_count; i++){
		printf("%s\n", listo->names[i]);
	}
	return NULL;
}
void* list_get_index(List *listo, int indx){
	if(indx < listo->item_count) {
		return listo->items[indx];
	}
	return NULL;
}
void* list_get_item(List *listo, void *item_to_find){
	for(int i=0; i < listo->item_count; i++){
		if(listo->items[i] == item_to_find) {
			return listo->items[i];
		}
	}
	return NULL;
}
void* list_get_name(List *listo, char *name_to_find){
	for(int i=0; i < listo->item_count; i++){
		if(strncmp(listo->names[i], name_to_find, strlen(name_to_find)) == 0) {
            return listo->items[i];
		}
	}
	return NULL;
}
int list_find(List *listo, char *name_to_find){
    for(int i=0; i < listo->item_count; i++){
		if(strncmp(listo->names[i], name_to_find, strlen(name_to_find)) == 0) {
            return i;
		}
	}
	return -1;
}
void list_delete_item(List *listo, void *item){
	for(int i=0; i < listo->item_count; i++){
		if( listo->items[i] == item ) {
            list_delete_index(listo, i);
		}
	}
}
void list_delete_name(List *listo, char *name) {
	int item_name;
	if(name == NULL) return;

	for(int i=0; i < listo->item_count; i++) {
		item_name = strlen(name);
		if( name != NULL && (strncmp(listo->names[i], name, strlen(name)) == 0) ) {
            list_delete_index(listo, i);
		}
	}
}
void list_delete_index(List *listo, int indx) {
	//remove item
	if(listo->names[indx] != NULL) {
        free(listo->names[indx]);
	}
	//restructure
	for(int j=indx; j < listo->item_count-1; j++) {
		listo->names[j] = listo->names[j+1];
		listo->items[j] = listo->items[j+1];
	}
	listo->item_count--;
	return;
}
void list_delete_all(List *listo) {
	for(int i=listo->item_count-1; i>=0; i--) {
        list_delete_index(listo, i);
	}
}
void list_free(List *listo) {
	list_delete_all(listo);
	free(listo->names);
	free(listo->items);
}
void list_print_list(List *listo){
	printf("count: %i/%i\n", listo->item_count, listo->current_max_size);
	for(int i=0; i < listo->item_count; i++) {
		printf("List[%i]: %s\n", i, listo->names[i]);
	}
}

RgbImage::RgbImage( unsigned long numRows, unsigned long numCols ){
   NumRows = numRows;
   NumCols = numCols;
   ImagePtr = new unsigned char[NumRows*GetNumBytesPerRow()];
   if ( !ImagePtr ) {
      fprintf(stderr, "Unable to allocate memory for %ld x %ld bitmap.\n", NumRows, NumCols);
      Reset();
      ErrorCode = MemoryError;
   }
   // Zero out the image
   unsigned char* c = ImagePtr;
   unsigned long rowLen = GetNumBytesPerRow();
   for ( unsigned long i=0; i<NumRows; i++ ) {
      for ( unsigned long j=0; j<rowLen; j++ ) {
         *(c++) = 0;
      }
   }
}
bool RgbImage::LoadBmpFile( const char* filename ){
   Reset();
   FILE* infile = fopen( filename, "rb" );      // Open for reading binary data
   if ( !infile ) {
      fprintf(stderr, "Unable to open file: %s\n", filename);
      ErrorCode = OpenError;
      return false;
   }

   bool fileFormatOK = false;
   int bChar = fgetc( infile );
   int mChar = fgetc( infile );
   if ( bChar=='B' && mChar=='M' ) {         // If starts with "BM" for "BitMap"
      skipChars( infile, 4+2+2+4+4 );         // Skip 4 fields we don't care about
      NumCols = readLong( infile );
      NumRows = readLong( infile );
      skipChars( infile, 2 );               // Skip one field
      int bitsPerPixel = readShort( infile );
      skipChars( infile, 4+4+4+4+4+4 );      // Skip 6 more fields

      if ( NumCols>0 && NumCols<=100000 && NumRows>0 && NumRows<=100000 && bitsPerPixel==24 && !feof(infile) ) {
         fileFormatOK = true;
      }
   }
   if (!fileFormatOK) {
      Reset();
      ErrorCode = FileFormatError;
      fprintf(stderr, "Not a valid 24-bit bitmap file: %s.\n", filename);
      fclose ( infile );
      return false;
   }

   // Allocate memory
   ImagePtr = new unsigned char[NumRows*GetNumBytesPerRow()];
   if ( !ImagePtr ) {
      fprintf(stderr, "Unable to allocate memory for %ld x %ld bitmap: %s.\n",
            NumRows, NumCols, filename);
      Reset();
      ErrorCode = MemoryError;
      fclose ( infile );
      return false;
   }

   unsigned char* cPtr = ImagePtr;
   for ( int i=0; i<NumRows; i++ ) {
      int j;
      for ( j=0; j<NumCols; j++ ) {
         *(cPtr+2) = fgetc( infile );   // Blue color value
         *(cPtr+1) = fgetc( infile );   // Green color value
         *cPtr = fgetc( infile );      // Red color value
         cPtr += 3;
      }
      int k=3*NumCols;               // Num bytes already read
      for ( ; k<GetNumBytesPerRow(); k++ ) {
         fgetc( infile );            // Read and ignore padding;
         *(cPtr++) = 0;
      }
   }
   if ( feof( infile ) ) {
      fprintf( stderr, "Premature end of file: %s.\n", filename );
      Reset();
      ErrorCode = ReadError;
      fclose ( infile );
      return false;
   }
   fclose( infile );   // Close the file
   return true;
}
short RgbImage::readShort( FILE* infile ) {
   // read a 16 bit integer
   unsigned char lowByte, hiByte;
   lowByte = fgetc(infile);         // Read the low order byte (little endian form)
   hiByte = fgetc(infile);         // Read the high order byte
   // Pack together
   short ret = hiByte;
   ret <<= 8;
   ret |= lowByte;
   return ret;
}
long RgbImage::readLong( FILE* infile ){
   // Read in 32 bit integer
   unsigned char byte0, byte1, byte2, byte3;
   byte0 = fgetc(infile);         // Read bytes, low order to high order
   byte1 = fgetc(infile);
   byte2 = fgetc(infile);
   byte3 = fgetc(infile);
   // Pack together
   long ret = byte3;
   ret <<= 8;
   ret |= byte2;
   ret <<= 8;
   ret |= byte1;
   ret <<= 8;
   ret |= byte0;
   return ret;
}
void RgbImage::skipChars( FILE* infile, int numChars ){
   for ( int i=0; i<numChars; i++ ) {
      fgetc( infile );
   }
}
void RgbImage::writeLong( long data, FILE* outfile ){
   // Read in 32 bit integer
   unsigned char byte0, byte1, byte2, byte3;
   byte0 = (unsigned char)(data&0x000000ff);      // Write bytes, low order to high order
   byte1 = (unsigned char)((data>>8)&0x000000ff);
   byte2 = (unsigned char)((data>>16)&0x000000ff);
   byte3 = (unsigned char)((data>>24)&0x000000ff);
   fputc( byte0, outfile );
   fputc( byte1, outfile );
   fputc( byte2, outfile );
   fputc( byte3, outfile );
}
void RgbImage::writeShort(short data, FILE* outfile) {
    unsigned char byte0, byte1;
    byte0 = data&0x000000ff;
    byte1 = (data>>8)&0x000000ff;
    fputc( byte0, outfile );
    fputc( byte1, outfile );
}
/*********************************************************************
* SetRgbPixel routines allow changing the contents of the RgbImage. *
*********************************************************************/
void RgbImage::SetRgbPixelf( long row, long col, double red, double green, double blue ){
   SetRgbPixelc( row, col, doubleToUnsignedChar(red),
                     doubleToUnsignedChar(green),
                     doubleToUnsignedChar(blue) );
}
void RgbImage::SetRgbPixelc( long row, long col,unsigned char red, unsigned char green, unsigned char blue ) {
   assert ( row<NumRows && col<NumCols );
   unsigned char* thePixel = GetRgbPixel( row, col );
   *(thePixel++) = red;
   *(thePixel++) = green;
   *(thePixel) = blue;
}
unsigned char RgbImage::doubleToUnsignedChar( double x ){
   if ( x>=1.0 ) {
      return (unsigned char)255;
   }
   else if ( x<=0.0 ) {
      return (unsigned char)0;
   }
   else {
      return (unsigned char)(x*255.0);      // Rounds down
   }
}
bool RgbImage::LoadFromOpenglBuffer() {             // Load the bitmap from the current OpenGL buffer
   int viewportData[4];
   glGetIntegerv( GL_VIEWPORT, viewportData );
   int& vWidth = viewportData[2];
   int& vHeight = viewportData[3];
   if ( ImagePtr==0 ) { // If no memory allocated
      NumRows = vHeight;
      NumCols = vWidth;
      ImagePtr = new unsigned char[NumRows*GetNumBytesPerRow()];
      if ( !ImagePtr ) {
         fprintf(stderr, "Unable to allocate memory for %ld x %ld buffer.\n", NumRows, NumCols);
         Reset();
         ErrorCode = MemoryError;
         return false;
      }
   }
   assert ( vWidth>=NumCols && vHeight>=NumRows );
   int oldGlRowLen;
   if ( vWidth>=NumCols ) {
      glGetIntegerv( GL_UNPACK_ROW_LENGTH, &oldGlRowLen );
      glPixelStorei( GL_UNPACK_ROW_LENGTH, NumCols );
   }
   glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

   // Get the frame buffer data.
   glReadPixels( 0, 0, NumCols, NumRows, GL_RGB, GL_UNSIGNED_BYTE, ImagePtr);

   // Restore the row length in glPixelStorei  (really ought to restore alignment too).
   if ( vWidth>=NumCols ) {
      glPixelStorei( GL_UNPACK_ROW_LENGTH, oldGlRowLen );
   }
   return true;
}
RgbImage::RgbImage() {
   NumRows = 0;
   NumCols = 0;
   ImagePtr = 0;
   ErrorCode = 0;
}
RgbImage::RgbImage( const char* filename ) {
   NumRows = 0;
   NumCols = 0;
   ImagePtr = 0;
   ErrorCode = 0;
   LoadBmpFile(filename);
}
RgbImage::~RgbImage() {
   delete[] ImagePtr;
}
 const unsigned char* RgbImage::GetRgbPixel( long row, long col ) const {
   assert ( row<NumRows && col<NumCols );
   const unsigned char* ret = ImagePtr;
   long i = row*GetNumBytesPerRow() + 3*col;
   ret += i;
   return ret;
}
 unsigned char* RgbImage::GetRgbPixel( long row, long col ) {
    assert (row<NumRows && col<NumCols);
    unsigned char* ret = ImagePtr;
    long i = row*GetNumBytesPerRow() + 3*col;
    ret += i;
    return ret;
}
 void RgbImage::GetRgbPixel( long row, long col, float* red, float* green, float* blue ) const {
    assert ( row<NumRows && col<NumCols );
    const unsigned char* thePixel = GetRgbPixel( row, col );
    const float f = 1.0f/255.0f;
    *red = f*(float)(*(thePixel++));
    *green = f*(float)(*(thePixel++));
    *blue = f*(float)(*thePixel);
}
 void RgbImage::GetRgbPixel( long row, long col, double* red, double* green, double* blue ) const {
    assert ( row<NumRows && col<NumCols );
    const unsigned char* thePixel = GetRgbPixel( row, col );
    const double f = 1.0/255.0;
    *red = f*(double)(*(thePixel++));
    *green = f*(double)(*(thePixel++));
    *blue = f*(double)(*thePixel);
}
 void RgbImage::Reset(){
    NumRows = 0;
    NumCols = 0;
    delete[] ImagePtr;
    ImagePtr = 0;
    ErrorCode = 0;
}
bool RgbImage::WriteBmpFile( const char* filename ){
   FILE* outfile = fopen( filename, "wb" );      // Open for reading binary data
   if ( !outfile ) {
      fprintf(stderr, "Unable to open file: %s\n", filename);
      ErrorCode = OpenError;
      return false;
   }
   fputc('B',outfile);
   fputc('M',outfile);
   int rowLen = GetNumBytesPerRow();
   writeLong( 40+14+NumRows*rowLen, outfile );   // Length of file
   writeShort( 0, outfile );               // Reserved for future use
   writeShort( 0, outfile );
   writeLong( 40+14, outfile );            // Offset to pixel data
   writeLong( 40, outfile );               // header length
   writeLong( NumCols, outfile );            // width in pixels
   writeLong( NumRows, outfile );            // height in pixels (pos for bottom up)
   writeShort( 1, outfile );      // number of planes
   writeShort( 24, outfile );      // bits per pixel
   writeLong( 0, outfile );      // no compression
   writeLong( 0, outfile );      // not used if no compression
   writeLong( 0, outfile );      // Pixels per meter
   writeLong( 0, outfile );      // Pixels per meter
   writeLong( 0, outfile );      // unused for 24 bits/pixel
   writeLong( 0, outfile );      // unused for 24 bits/pixel

   // Now write out the pixel data:
   unsigned char* cPtr = ImagePtr;
   for ( int i=0; i<NumRows; i++ ) {
      // Write out i-th row's data
      int j;
      for ( j=0; j<NumCols; j++ ) {
         fputc( *(cPtr+2), outfile);      // Blue color value
         fputc( *(cPtr+1), outfile);      // Blue color value
         fputc( *(cPtr+0), outfile);      // Blue color value
         cPtr+=3;
      }
      // Pad row to word boundary
      int k=3*NumCols;               // Num bytes already read
      for ( ; k<GetNumBytesPerRow(); k++ ) {
         fputc( 0, outfile );            // Read and ignore padding;
         cPtr++;
      }
   }

   fclose( outfile );   // Close the file
   return true;
}

// Ultil
void printVector(ObjVector *v) {
	//cout << "(" << v->e[0] << " " << v->e[1] << " " << v->e[2] << ") ";
}
void printObjDetails(ObjLoader* objData) {
    //cout << "Number of vertices: " << objData->vertexCount << endl;
	//cout << "Number of vertex normals: " << objData->normalCount << endl;
	//cout << "Number of texture coordinates:" << objData->textureCount << endl << endl;

	//cout << "Number of faces: " << objData->faceCount << endl;
	for(int i=0; i<objData->faceCount; i++) {
		ObjFace *o = objData->faceList[i];
		//cout << " mtl: " << o->material_index;
		//cout << " face ";
		for(int j=0; j<3; j++) {
			printVector(objData->vertexList[ o->vertex_index[j] ]);
		}
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of spheres: " << objData->sphereCount << endl;
	for(int i=0; i<objData->sphereCount; i++) {
		ObjSphere *o = objData->sphereList[i];
		//cout << " sphere ";
		printVector(objData->vertexList[ o->pos_index ]);
		printVector(objData->normalList[ o->up_normal_index ]);
		printVector(objData->normalList[ o->equator_normal_index ]);
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of planes: " << objData->planeCount << endl;
	for(int i=0; i<objData->planeCount; i++){
		ObjPlane *o = objData->planeList[i];
		//cout << " plane ";
		printVector(objData->vertexList[ o->pos_index ]);
		printVector(objData->normalList[ o->normal_index]);
		printVector(objData->normalList[ o->rotation_normal_index]);
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of point lights: " << objData->lightPointCount << endl;
	for(int i=0; i<objData->lightPointCount; i++){
		ObjLightPoint *o = objData->lightPointList[i];
		//cout << " plight ";
		printVector(objData->vertexList[ o->pos_index ]);
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of disc lights: " << objData->lightDiscCount << endl;
	for(int i=0; i<objData->lightDiscCount; i++){
		ObjLightDisc *o = objData->lightDiscList[i];
		//cout << " dlight ";
		printVector(objData->vertexList[ o->pos_index ]);
		printVector(objData->normalList[ o->normal_index ]);
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of quad lights: " << objData->lightQuadCount << endl;
	for(int i=0; i<objData->lightQuadCount; i++){
		ObjLightQuad *o = objData->lightQuadList[i];
		//cout << " qlight ";
		printVector(objData->vertexList[ o->vertex_index[0] ]);
		printVector(objData->vertexList[ o->vertex_index[1] ]);
		printVector(objData->vertexList[ o->vertex_index[2] ]);
		printVector(objData->vertexList[ o->vertex_index[3] ]);
		//cout << endl;
	}
	//cout << endl;

	if(objData->camera != NULL){
		//cout << "Found a camera\n";
		//cout << " position: ";
		printVector(objData->vertexList[ objData->camera->camera_pos_index ]);
		//cout << "\n looking at: ";
		printVector(objData->vertexList[ objData->camera->camera_look_point_index ]);
		//cout << "\n up normal: ";
		printVector(objData->normalList[ objData->camera->camera_up_norm_index ]);
		//cout << endl;
	}
	//cout << endl;

	//cout << "Number of materials: " << objData->materialCount << endl;
	for(int i=0; i<objData->materialCount; i++){
		ObjMaterial *mtl = objData->materialList[i];
		//cout << " name: " << mtl->name;
		//cout << " amb: " << mtl->amb[0] << " " << mtl->amb[1] << " " << mtl->amb[2] << endl;
		//cout << " diff: " << mtl->diff[0] << " " << mtl->diff[1] << " " << mtl->diff[2] << endl;
		//cout << " spec: " << mtl->spec[0] << " " << mtl->spec[1] << " " << mtl->spec[2] << endl;
		//cout << " reflect: " << mtl->reflect << endl;
		//cout << " trans: " << mtl->trans << endl;
		//cout << " glossy: " << mtl->glossy << endl;
		//cout << " shiny: " << mtl->shiny << endl;
		//cout << " refact: " << mtl->refract_index << endl;
		//cout << " texture: " << mtl->texture_filename << endl << endl;
	}
	//cout << endl;
}
void loadTextureFromFile(ObjLoader* obj, char *filename, int index) {
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    RgbImage theTexMap(filename);
    glGenTextures(1, &obj->objTexture[index]);
    glBindTexture(GL_TEXTURE_2D, obj->objTexture[index]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, 3, theTexMap.GetNumCols(), theTexMap.GetNumRows(), 1, GL_RGB, GL_UNSIGNED_BYTE, theTexMap.ImageData());
}
void loadTextureToObject(ObjLoader* obj) {
	//cout << endl << obj->materialCount << endl << endl;
	char rr[1000];
	string s;
    for (int i=0; i<obj->materialCount; i++) {
        //cout << obj->materialList[i]->texture_filename << "!!!!" << endl;
        s = obj->materialList[i]->texture_filename;
        //cout << "Material " << i << " :" << s << "Leng: " << s.length() << endl;
        for(int j=0; j<s.length()-1; j++) {
            //cout << j << " " << s[j];
            rr[j] = s[j];
            //cout << " OK" << endl;
        }
        rr[s.length()-1] = '\0';
        char* r = rr;
        loadTextureFromFile(obj, r, i);
        //cout << "Load " << r << " ok\n";
    }
}
void createObject(vector<ObjLoader*>& objList, char* filename, Coordinates* coor, GLfloat minimizeRate, GLfloat angel) {
    ObjLoader* obj = new ObjLoader(filename, coor, minimizeRate, angel);
    //cout << "LOAD OBJECT: " << filename << " OK" << endl;
    objList.push_back(obj);
    loadTextureToObject(obj);
    //cout << "LOAD TEXTURE: " << filename << " OK" << endl;
}
void *playSound(void *threadid){
    PlaySound("Sounds/visit.WAV", NULL, SND_FILENAME);
    pthread_exit(NULL);
}
void startSound() {
    pthread_t thread;
    pthread_create(&thread, NULL, playSound, (void *)0);
}
bool enterFullscreen(HWND hwnd, int fullscreenWidth, int fullscreenHeight, int colourBits, int refreshRate) {
    DEVMODE fullscreenSettings;
    bool isChangeSuccessful;
    RECT windowBoundary;
    EnumDisplaySettings(NULL, 0, &fullscreenSettings);
    fullscreenSettings.dmPelsWidth = fullscreenWidth;
    fullscreenSettings.dmPelsHeight = fullscreenHeight;
    fullscreenSettings.dmBitsPerPel = colourBits;
    fullscreenSettings.dmDisplayFrequency = refreshRate;
    fullscreenSettings.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL | DM_DISPLAYFREQUENCY;
    SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);
    SetWindowLongPtr(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);
    SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, fullscreenWidth, fullscreenHeight, SWP_SHOWWINDOW);
    isChangeSuccessful = ChangeDisplaySettings(&fullscreenSettings, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL;
    ShowWindow(hwnd, SW_MAXIMIZE);
    return isChangeSuccessful;
}
bool exitFullscreen(HWND hwnd, int windowX, int windowY, int windowedWidth, int windowedHeight, int windowedPaddingX, int windowedPaddingY) {
    bool isChangeSuccessful;
    SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_LEFT);
    SetWindowLongPtr(hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
    isChangeSuccessful = ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL;
    SetWindowPos(hwnd, HWND_NOTOPMOST, windowX, windowY, windowedWidth + windowedPaddingX, windowedHeight + windowedPaddingY, SWP_SHOWWINDOW);
    ShowWindow(hwnd, SW_RESTORE);
    return isChangeSuccessful;
}
