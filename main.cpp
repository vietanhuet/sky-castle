#include <iostream>
#include <vector>
#include <windows.h>
#include <gl/glut.h>
#include "SkyCity.h"
using namespace std;

GLfloat xRotated, yRotated, zRotated;
GLfloat xLookat = 200, yLookat = 0, zLookat = 200;

void init(){
    glEnable(GL_TEXTURE_2D);
    glClearColor(0.3, 0.7, 0.6, 0.5);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
}

vector<ObjLoader*> objList; // THE MOST IMPORTANT !!
void drawObjList() {
    for(unsigned long k=0; k<objList.size(); k++) {
        glPushMatrix();
        ObjLoader* obj = objList[k];
        for(unsigned long i=0; i<obj->faceCount; i++){
            ObjFace *o = obj->faceList[i];
            glBindTexture(GL_TEXTURE_2D, obj->objTexture[o->material_index]);
            if(o->vertex_count == 3) {
                glBegin(GL_TRIANGLES);
            } else
            if(o->vertex_count == 4) {
                glBegin(GL_QUADS);
            }
            for(unsigned long j=0; j<o->vertex_count; j++) {
                glTexCoord2f(obj->textureList[o->texture_index[j]]->e[0], obj->textureList[o->texture_index[j]]->e[1]);
                glNormal3f(
                    (obj->normalList[o->normal_index[j]]->e[0]) ,
                    (obj->normalList[o->normal_index[j]]->e[1]) ,
                    (obj->normalList[o->normal_index[j]]->e[2]));
                glVertex3f(
                    (obj->vertexList[o->vertex_index[j]]->e[0]) * obj->minimizeRate + obj->coor->x,
                    (obj->vertexList[o->vertex_index[j]]->e[1]) * obj->minimizeRate + obj->coor->y,
                    (obj->vertexList[o->vertex_index[j]]->e[2]) * obj->minimizeRate + obj->coor->z);
            }
            glEnd();
        }
        glPopMatrix();
    }
}
void createObjList() {
    //createObject(objList, "Objects/trollwatchtower/trollwatchtower.obj", new Coordinates(150, -8, 120), 2.0, 0.0f);
    createObject(objList,"Objects/westfallchurch/westfallchurch.obj", new Coordinates(0, -10, 0), 6.0, 0.0f);
    //createObject(objList,"Objects/Banana/Banana.obj", new Coordinates(25, -15, 85), 0.05, 0.0f);
   // createObject(objList,"Objects/Boxes/Boxes.obj", new Coordinates(5, -15, 85), 0.2, 0.0f);
    //createObject(objList,"Objects/windmill/westfallwindmill.obj", new Coordinates(135, -15, 165), 1.0, 0.0f);
   // createObject(objList,"Objects/oilrig/oilrig.obj", new Coordinates(85, -15, 85), 1.0, 0.0f);
   // createObject(objList,"Objects/oilrig/oilrig.obj", new Coordinates(-85, -15, 85), 1.0, 0.0f);
    createObject(objList,"Objects/oilrig/oilrig.obj", new Coordinates(270, -35, 50), 2.0, 0.0f);
    createObject(objList,"Objects/Sidestep/Sidestep.obj", new Coordinates(250, -35, 50), 0.5, 0.0f);
  //  createObject(objList,"Objects/oilrig/oilrig.obj", new Coordinates(85, -15, -85), 1.0, 0.0f);
    //createObject(objList,"Objects/TropicalIsland/SmallTropicalIsland.obj", new Coordinates(0, -15, 0), 0.5, 0.0f);
    createObject(objList,"Objects/BigSkyIsland/BigSkyIsland.obj", new Coordinates(0, -115, 0), 2, 0.0f);
    createObject(objList,"Objects/SmallSkyIsland/SmallSkyIsland.obj", new Coordinates(270, -55, 50), 1, 0.0f);
    createObject(objList,"Objects/Stars/nexusraid_nebulasky.obj", new Coordinates(0, 80, 0), 12, 0.0f);
    //createObject(objList, "Objects/Skybox/diremaulskybox.obj", new Coordinates(0, -15, 0), 5, 0.0f);
   // createObject(objList, "Objects/Castle/castle01.obj", new Coordinates(0, 0, 0), 0.02, 0.0f);
    startSound();
}

void display(){
    glEnable(GL_COLOR_MATERIAL);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
  //  glPushMatrix();
    gluLookAt(xLookat, yLookat, zLookat, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

    drawObjList();
 //   glPopMatrix();
    glFlush();
    glutSwapBuffers();
}
void startVisiting() {
    while(true) {
        Sleep(1);
        xLookat -= 0.025;
        yLookat -= 0.02;
        zLookat -= 0.03;
        display();
    }
}
void idleFunc() {
    //startVisiting();
}
void passiveMotion(int x, int y) {
    //cout << x << " " << y << endl;
}
void keyboard (unsigned char key, int x, int y) {
   switch (key) {
      case 27:
         exit(0);
         break;
      default:
         break;
   }
}
void keyboardSpecial (int key, int x, int y) {
    switch(key) {
    case GLUT_KEY_F1:
        yLookat -= 5;
        display();
        break;
    case GLUT_KEY_F2:
        yLookat += 5;
        display();
        break;
    case GLUT_KEY_UP:
        xLookat += 5;
        display();
        break;
    case GLUT_KEY_DOWN:
        xLookat -= 5;
        display();
        break;
    case GLUT_KEY_LEFT:
        zLookat -= 5;
        display();
        break;
    case GLUT_KEY_RIGHT:
        zLookat += 5;
        display();
        break;
    default:
        break;
    }
}
void resizeWindow(int x, int y) {
    if (x == 0) {
		y = 1;
	}
	glViewport(0, 0, x, y);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)x/(GLfloat)y,0.1f,1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(1366, 768);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Sky City");


    createObjList();


    enterFullscreen(FindWindow("GLUT","Sky City"),
        GetDeviceCaps(GetDC(FindWindow("GLUT","Sky City")), HORZRES),
        GetDeviceCaps(GetDC(FindWindow("GLUT","Sky City")), VERTRES),
        GetDeviceCaps(GetDC(FindWindow("GLUT","Sky City")), BITSPIXEL),
        GetDeviceCaps(GetDC(FindWindow("GLUT","Sky City")), VREFRESH));
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(resizeWindow);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(keyboardSpecial);
    glutPassiveMotionFunc(passiveMotion);
  	glutIdleFunc(idleFunc);
    glutMainLoop();
    pthread_exit(NULL);
	return 0;
}
